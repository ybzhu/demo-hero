import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
// 国际化要用到的库
// import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
// import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// 当应用规模还是中小型时，路由配置可以重构成一个单独的文件，在app.module中导入就行了
//import { AppRoutingModule } from './app-routing.module';

// 当应用规模较大，路由配置细致复杂时，可以重构成routes.module，依旧在app.module中导入就行了
import { RoutesModule } from './routes/routes.module';
import { HeroService } from './service/hero.service';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './service/in-memory-data.service';

import { AppComponent } from './app.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroesComponent } from './heroes/heroes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { Form1Component } from './form1/form1.component';


// export function createTranslateLoader(http: Http) {
//     return new TranslateHttpLoader(http);
// }

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent,
    HeroesComponent,
    DashboardComponent,
    HeroSearchComponent,
    Form1Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,  //响应式表单要用到
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    //AppRoutingModule,
    
    // TranslateModule.forRoot({
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: createTranslateLoader,
    //     deps: [Http]
    //   }
    // }),
    RoutesModule
  ],
  providers: [HeroService],
  bootstrap: [AppComponent]
})
export class AppModule { }
