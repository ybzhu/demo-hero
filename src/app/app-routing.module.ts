// 已经重构为 routes.module ，本路由配置文件可以删除
// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';

// import { HeroDetailComponent } from './hero-detail/hero-detail.component';
// import { HeroesComponent } from './heroes/heroes.component';
// import { DashboardComponent } from './dashboard/dashboard.component';

// const routes: Routes = [
// 	{
// 	  path: 'heroes',
// 	  component: HeroesComponent
// 	},
// 	{
// 	  path: 'dashboard',
// 	  component: DashboardComponent
// 	},
// 	{
// 	  path: 'detail/:id',
// 	  component: HeroDetailComponent
// 	},
// 	{
// 	  path: '',
// 	  redirectTo: '/dashboard',
// 	  pathMatch: 'full'
// 	}
// ];

// @NgModule({
// 	imports: [RouterModule.forRoot(routes)],
// 	exports: [RouterModule]
// })

// export class AppRoutingModule {}