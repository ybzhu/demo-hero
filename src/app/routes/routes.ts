import { HeroesComponent } from '../heroes/heroes.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HeroDetailComponent } from '../hero-detail/hero-detail.component';
import { Form1Component } from '../form1/form1.component';

export const routes = [
	{
	  path: 'heroes',
	  component: HeroesComponent
	},
	{
	  path: 'dashboard',
	  component: DashboardComponent
	},
	{
	  path: 'form1',
	  component: Form1Component
	},
	{
	  path: '',
	  redirectTo: '/dashboard',
	  pathMatch: 'full'
	},
	{
	  path: 'detail/:id',
	  component: HeroDetailComponent
	}
];