import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Hero } from '../classes/hero.class';
import { HEROES } from '../mock/hero.data';

@Injectable()
export class HeroService {
	private heroesUrl = 'api/heroes';  // URL to web api

  constructor(private http: Http) { }

  // 模拟http请求取代直接返回硬编码的数据
  // getHeroes(): Promise<Hero[]> {
  // 	return Promise.resolve(HEROES);
  // }

  getHeroes(): Promise<Hero[]> {
  	return this.http.get(this.heroesUrl)
  							.toPromise()
  							.then(response => response.json().data as Hero[])
              	.catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
  	console.log('http 请求数据发生错误： ', error);
  	return Promise.reject(error.message || error);
  }


 //  getHeroesSlowly(): Promise<Hero[]> {
 //  	console.log('waiting');
	//   return new Promise(resolve => {
	//     setTimeout(() => resolve(this.getHeroes()), 2000);
	//   });
	// }

	// 模拟http请求取代直接返回硬编码的数据
	// getHero(id: number): Promise<Hero> {
	//   return this.getHeroes()
	//              .then(heroes => heroes.find(hero => hero.id === id));
	// }

	getHero(id: number): Promise<Hero> {
		const url = `${this.heroesUrl}/${id}`;
	  return this.http.get(url)
	    .toPromise()
	    .then(response => response.json().data as Hero)
	    .catch(this.handleError);
	}

	private headers = new Headers({'Content-Type': 'application/json'});

	update(hero: Hero): Promise<Hero> {
	  const url = `${this.heroesUrl}/${hero.id}`;
	  return this.http
	    .put(url, JSON.stringify(hero))
	    .toPromise()
	    .then(() => hero)
	    .catch(this.handleError);
	}

	create(name: string): Promise<Hero> {
	  return this.http
	    .post(this.heroesUrl, JSON.stringify({name: name}))
	    .toPromise()
	    .then(res => res.json().data as Hero)
	    .catch(this.handleError);
	}

	delete(id: number): Promise<void> {
	  const url = `${this.heroesUrl}/${id}`;
	  return this.http.delete(url)
	    .toPromise()
	    .then(() => null)
	    .catch(this.handleError);
	}

}
