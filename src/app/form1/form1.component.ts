import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component implements OnInit {
	heroForm = new FormGroup ({
    name: new FormControl('', Validators.required)
  });

  heroForm2: FormGroup;

  constructor(private fb: FormBuilder) { 
  	this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
  	this.heroForm2 = this.fb.group({
  		mobile: ['', Validators.required],
  		age: '',
  		sex: ['', Validators.required],
  	});
  }

}
