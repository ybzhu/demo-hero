import { DemoHeroPage } from './app.po';

describe('demo-hero App', () => {
  let page: DemoHeroPage;

  beforeEach(() => {
    page = new DemoHeroPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
