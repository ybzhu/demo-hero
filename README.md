## DemoHero


这是一个按照 [官方教程](http://angular2.axuer.com/docs/ts/latest/tutorial/) 搭建的 angular2 demo。效果图如下：

![效果图](http://angular2.axuer.com/resources/images/devguide/toh/toh-anim.gif)

搭建过程是逐渐深入了解 angular2 的过程：

1. 双向绑定
2. 在html中使用指令（directives）
3. 父子组件及父组件向子组建通信
4. 创建服务(指那些能够被其它的组件或者指令调用的 单一的,可共享的代码块)
5. 添加路由（简单路由、重定向路由、参数化路由、回到原路）
6. 使用 http 服务从服务器获取数据



### service（服务）

在Angular中，我们所说的服务是指那些能够被其它的组件或者指令调用的单一的，可共享的代码块。服务能够使我们提高代码的利用率，方便组件之间共享数据和方法，方便测试和维护。

在这个 demo 中，先后用不同方式实现了两个重要的服务

1. http请求 hero 数据
2. http请求 搜索 数据


以请求 hero 数据为例，说明一个服务的创建过程。  
在组件 heroes、hero-detail、dashboard 中都要使用到 hero 数据，  
有的组件要 hero 列表数据，有的组件要特定 hero 对象的数据，有的组件要编辑提交特定 hero 对象。  
此时就可以将 hero 数据相关的功能提取出来重构成 `hero服务`，  
在需要 hero 数据的组件中注入 `hero服务` 就可以使用 `hero服务` 提供的方法。  
注意：当 `hero服务` 发生调整之后，组件照旧使用（DI）该服务（也许具体的方法有所变化）  



#### 1. 创建一个服务

看示例代码，可见服务只是 Angular 中的一个类。  
@Injectable() 标识一个类可以被注入器实例化  
此后在定义了依赖注入的地方，注入器会将服务注入到组件中  

```js
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

// 使用 @Injectable() 装饰器修饰 XxxxService 类
@Injectable()
export class XxxxService {
    // todo 本服务中具体的方法
}
```


#### 2. 注入服务

可在2个地方注入服务：模型（@NgModule）、组件（@Component）。  
两者作用域不同，在 @Component 中注册的只在该组件及其子组件中可用。  

```js
// 这段示例将 HeroService 注入到 模型中
import { NgModule } from '@angular/core';
import { HeroService } from './service/hero.service';

@NgModule({
  // ...
  providers: [
    HeroService
  ],
  // ...
})
```


#### 3. 使用服务

最佳实践告诉我们在 `组件类` 的构造函数中添加服务，以供组件类中的方法使用该服务（的方法）。




### route(路由)

Angular的路由器能让用户从一个视图导航到另一个视图。

要使用Angular的路由器，导入 RouterModule 和 Routes 基本就足够应付应用中所有路由配置了。  
RouterModule 本质是一个模型。  

#### 1. 如下将 URL 路由写入一个常量：

```js
import { Routes } from '@angular/router';
//...

// 不加 : Routes 约束数组的类型也没关系，因为 Routes 本质是一个可包含配置选项的数组
export const routes: Routes = [
    // 配置选项可以参考API
    // API链接：https://www.angular.cn/docs/ts/latest/api/router/index/Routes-type-alias.html
    {
      path: 'dashboard',
      component: DashboardComponent
    },
    {
      path: '',
      redirectTo: '/dashboard',
      pathMatch: 'full'
    }
];
```


#### 2. 在根组件模型中导入
要在应用中生效以上配置，只要用路由模型 RouterModule   将数组包裹，最终导入进根组件的模型（一般都是AppModule）中。  

```js
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
    // other imports here
  ],
  ...
})
export class AppModule { }
```


#### 3. 其它

还要在index.html的<head>标签下先添加一个<base>元素；

```html
<base href="/">
```

在 `根组件` 、 `布局组件` 或 `母组件` 添加 `RouterOutlet指令` ，目的是指定在该组件的这个位置显示路由器匹配到的组件或指令。

路由器是根据 URL 匹配的；  
直接键入 URL 就可以显示相应组件构成的视图；  
也可以通过 &lt;a&gt; 锚点跳转到预设好的视图。  

```html
<a routerLink="/dashboard" routerLinkActive="active">dashboard</a>
```


#### 4. 带参数的路由

带参数的路由还会用到 ActivatedRoute， Params， Router，  
demo 的 hero-detail 、hero-search 、heros 组件各自都有用到。  
具体[API参考](https://www.angular.cn/docs/ts/latest/api/#!?query=router)  



### HTTP请求

待完善


### Angular-cli

建议使用配套的脚手架工具 Angular-cli ，因为：

1. 使用 ng serve 命令调试项目依赖该工具；
2. 使用 ng new 可以快速搭好最初的项目，npm依赖会一并下载好；
3. ng generate component/directive/pipe/service/class/module 命令可快速生成组件/指令/管道/服务/类/模型；
4. 封装了 webpack 的功能


分析 `.angular-cli.json` 配置文件

```json
{
  // ...
  "apps": [
    {
      "root": "src",
      "outDir": "dist",
      "assets": [
        "assets",
        "favicon.ico"
      ],
      "index": "index.html",
      "main": "main.ts",   // 作为一个入口文件，最终打包成 main.bundle.js
      "polyfills": "polyfills.ts",   // 入口文件，打包成 polyfills.bundle.js
      "test": "test.ts",
      "tsconfig": "tsconfig.app.json",
      "testTsconfig": "tsconfig.spec.json",
      "prefix": "app",
      "styles": [   // 数组内容作为打包对象，最终打包成 styles.bundler.js
        "styles.css"
      ],
      "scripts": [],   // 将数组内容打包成 scripts.bundler.js。这个数组一般为依赖的js插件，如：jquery、bootstrap.js等
      "environmentSource": "environments/environment.ts",
      "environments": {
        "dev": "environments/environment.ts",
        "prod": "environments/environment.prod.ts"
      }
    }
  ],
  "e2e": {
    "protractor": {
      "config": "./protractor.conf.js"
    }
  },
  "lint": [
    {
      "project": "src/tsconfig.app.json"
    },
    {
      "project": "src/tsconfig.spec.json"
    },
    {
      "project": "e2e/tsconfig.e2e.json"
    }
  ],
  "test": {
    "karma": {
      "config": "./karma.conf.js"
    }
  },
  "defaults": {
    "styleExt": "css",
    "component": {}
  }
}

```

注意：除了 main.bundle.js 、 polyfills.bundle.js 、 styles.bundler.js 、 scripts.bundler.js 这4个文件外，angular2 和 webpack 的js文件会打包成 vendor.bundle.js。


## 国际化 i18n

angular2 推荐使用 ng2-translate ，[使用参考](https://github.com/ngx-translate/core/blob/fb02ca5920aae405048ebab50e09db67d5bf12a2/README.md#usage)
angular2 以后的版本推荐使用 @ngx-translate/core ，[使用参考](https://www.npmjs.com/package/@ngx-translate/core#usage)


## 表单

### 响应式表单

要创建响应式表单，需要 AppModule 导入 `ReactiveFormsModule` 。  

通常在组件类中会使用到3个表单类：`FormControl` ， `FormGroup` ， `FormArray` ， 他们都继承自基础类 `AbstractControl` ，会有一些表单属性。  

组件 form1 就是一个响应式表单的demo。

`FormBuilder` 封装了创建 `FormGroup` 和 `FormArray` 的方法。  

<img src="./src/assets/img/form/form-01-reactive.gif" alt="" width="400">

### 动态表单

需要编写大量表单时，基于业务对象模型的元数据，动态创建表单是不错的选择。
基于[官方范例](https://www.angular.cn/resources/live-examples/cb-dynamic-form/ts/eplnkr.html)，总结以下创建动态表单的步骤：

1. 定义一个对象模型，用来描述所有表单功能需要的场景。
2. 依照模型定义表单数据（后期是要通过向后抬请求获取的）  
3. 完善组件：用 FormBuilder 创建表单、用结构型指令(ngIf、ngFor、ngSwitch)协助渲染html页面  


以下是 EDC 系统初步的对象模型：  

```js
/**
 * EDC系统 数据点表单项 动态生成 base 类。
 * 需要其他类继承，以衍生出不同 type 属性的表单项。
 */
export class PointBase<T> {
  // 表单的标签名
  label: string;
  // 表单的默认值
  value: T;
  // 表单的name属性
  key: string;
  // type属性
  controlType: string;
  // 表单项之间的前后顺序
  order: number;
  // 编辑状态
  editable: boolean;

  constructor(options: {
      label?: string, 
      value?: T, 
      key?: string, 
      controlType?: string, 
      order?: number,
      editable?: boolean
    } = {}) {
    this.label = options.label || '';
    this.value = options.value;
    this.key = options.key || '';
    this.controlType = options.controlType || '';
    this.order = options.order === undefined ? 1 : options.order;
    this.editable = !!options.editable;
  }
}

/** 检查框／多选 */
export class PointCheckout extends PointBase<string> {
  controlType = 'checkbox';
  option: string;
  checked: boolean;
  
  constructor(options: {} = {}) {
    super(options);
    this.option = options['option'] || '';
    this.checked = !!options['checked'];
  }
}


/** 日期、时间框 */
export class PointDate extends PointBase<string> {
  // 类型一律为时间，通过 format 显示为日期或时间
  controlType = 'datetime';
  format: string;

  constructor(options: {} = {}) {
    super(options);
    this.format = options['format'] || 'yyyy-MM-dd';
  }
}

/** 文本标签 */
export class PointLabel extends PointBase<string> {
  controlType = 'label';

  constructor(options: {} = {}) {
    super(options);
  }
}

/** 单选 */
export class PointRadio extends PointBase<string> {
  controlType = 'radio';
  horizontal: boolean;
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    // 默认是竖直单选框
    this.horizontal = !!(options['horizontal']);
    this.options = options['options'] || [];
  }
}

/** 下拉选择 */
export class PointSelect extends PointBase<string> {
  controlType = 'select';
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}

/** 文本框 */
export class PointText extends PointBase<string> {
  controlType = 'text';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || 'text';
  }
}

/** 长文本 */
export class PointTextarea extends PointBase<string> {
  controlType = 'textarea';
  rows: number;

  constructor(options: {} = {}) {
    super(options);
    // 默认高度 3 行
    this.rows = options['rows'] || 3;
  }
}
```


一些常用组件的样式：  

<img src="./src/assets/img/form/form-02-dynamic.gif" alt="" width="400">

<img src="./src/assets/img/form/form-03-dynamic.gif" alt="" width="600">

新的的表单样式：一行内多个元素（数据点）

<img src="./src/assets/img/form/edc-01-demo_v2.gif" alt="" width="600">

<img src="./src/assets/img/form/edc-01-demo_v3.gif" alt="" width="600">


## Table

<img src="./src/assets/img/table/responsive-table.gif" alt="" width="600">




### 表单验证


## 数据存储

### Cookie
angular1.x的项目建议使用[ngx-cookie模块](https://github.com/salemdar/ngx-cookie);
angular2.x的项目建议使用[ng2-cookies模块](https://github.com/BCJTI/ng2-cookies);  
angular2+ 的项目可用的太多，没看。  

以上这些库的基本思路是：将涉及cookie的操作封装到服务中，形成cookieService供开发者使用。  
至于具体怎么使用操作cookie的服务，以上2个模块都有说明。  

大致过程是：
1. 注入服务。在模型 ngModule 的 providers 中注入
2. 在组件或其他服务中使用。（也可以直接在组件的 providers 注入）


### WebStorage

WebStorage 有2种：
1. localStorage，永久保存直到用户手动删除
2. sessionStorage，只在当前标签页有效

WebStorage 事件也有2种：
1. storage
2. change (未列入标准)

这有一个问题，在使用 sessionStorage 存储用户的敏感信息时，不支持跨标签页共享数据。  
下面是解决办法：[译文](http://blog.kazaff.me/2016/09/09/%E8%AF%91-%E5%9C%A8%E5%A4%9A%E4%B8%AA%E6%A0%87%E7%AD%BE%E9%A1%B5%E4%B9%8B%E9%97%B4%E5%85%B1%E4%BA%ABsessionStorage/?utm_source=tuicool&utm_medium=referral)




## 布局 demo

布局样式01-mobile  
![布局样式01-mobile](./src/assets/img/layout/layout-style-01-mobile.gif)

布局样式01-PC  
![布局样式01-PC](./src/assets/img/layout/layout-style-01-PC.gif)

布局样式02-mobile  
![布局样式02-mobile](./src/assets/img/layout/layout-style-02-mobile.gif)

布局样式02-PC  
![布局样式02-PC](./src/assets/img/layout/layout-style-02-PC.gif)





## 深入理解 angular

#### 组件继承

这一小节[参考文章](https://segmentfault.com/a/1190000008976996)《Angular 2 Component Inheritance》

组件继承涉及以下的内容：

* Metadata：如 @Input()、@Output()、@ContentChild/Children、@ViewChild/Children 等。在派生类中定义的元数据将覆盖继承链中的任何先前的元数据，否则将使用基类元数据。
* Constructor：如果派生类未声明构造函数，它将使用基类的构造函数。这意味着在基类构造函数注入的所有服务，子组件都能访问到。
* Lifecycle hooks：如果基类中包含生命周期钩子，如 ngOnInit、ngOnChanges 等。尽管在派生类没有定义相应的生命周期钩子，基类的生命周期钩子会被自动调用。

**注意：模板是不能被继承的** 


#### Renderer、ElementRef、TemplateRef、ViewRef 、ComponentRef 和 ViewContainerRef

这一小节参考文章[《Angular 2 TemplateRef & ViewContainerRef》](https://segmentfault.com/a/1190000008672478) 和 [《Angular 2 ElementRef》](https://segmentfault.com/a/1190000008653690)

这些类都是 core 库提供的，它们可以操作不同的元素：


1. [ElementRef](https://www.angular.cn/docs/ts/latest/api/core/index/ElementRef-class.html) 用于操作 DOM 元素。主要作用是封装不同平台下视图层中的 native 元素 (在浏览器环境中，native 元素通常是指 DOM 元素)
2. Renderer 抽象类封装了不同平台的差异，统一了 [API 接口](https://segmentfault.com/a/1190000008653690#articleHeader3)
2. [TemplateRef](https://www.angular.cn/docs/ts/latest/api/core/index/TemplateRef-class.html) 用于表示内嵌的 &lt;template&gt; 模板元素。主要作用是创建 嵌入式视图(EmbeddedViewRef) 和 访问 DOM 元素
3. [ViewContainerRef](https://www.angular.cn/docs/ts/latest/api/core/index/ViewContainerRef-class.html) 用于表示一个视图容器，可添加一个或多个视图。主要作用是创建和管理 嵌入式视图 和 组件视图（ComponentRef）
4. ViewRef 用于表示 Angular View(视图)，视图是可视化的 UI 界面。EmbeddedViewRef 就继承于 ViewRef


### 属性装饰器

#### @ContentChild & @ContentChildren 

这一小节[参考文章](https://segmentfault.com/a/1190000008707828#articleHeader2)《Angular 2 ContentChild & ContentChildren》

常常和 ng-content 一起使用。  
一般在子组件模版中使用 ng-content 标签，
这样可以将父组件中的内容加（转移）到 ng-content 标签所在位置。
默认内容是父组件中子组件的标签，此外，ng-content 标签的 select 属性也可以选择父组件的指定内容，指定方式如同css选择器，如:"#id",".class","[name=value]"等


1. ContentChild 属性装饰器，用来从通过 Content Projection 方式 (ng-content) 设置的视图中获取匹配的元素。
2. ContentChildren 属性装饰器，用来从通过 Content Projection 方式 (ng-content) 设置的视图中获取匹配的多个元素。返回的结果是一个 QueryList 集合。

**注意区别**： 

1. ViewChild 是用来从**模板视图**中获取匹配的元素。
2. 在父组件的 ngAfterContentInit 生命周期钩子中才能成功获取通过 ContentChild 查询的元素。
3. 在父组件的 ngAfterViewInit 生命周期钩子中才能成功获取通过 ViewChild 查询的元素。

child.component.ts
```typescript
import { Component } from '@angular/core';

@Component({
    selector: 'exe-child',
    template: `
      <p>Child Component</p>  
    `
})
export class ChildComponent {
    name: string = 'child-component';
}
```

parent.component.ts
```typescript
import { Component, ContentChild, AfterContentInit } from '@angular/core';
import { ChildComponent } from './child.component';

@Component({
    selector: 'exe-parent',
    template: `
      <p>Parent Component</p>  
      <ng-content></ng-content>
    `
})
export class ParentComponent implements AfterContentInit {
    @ContentChild(ChildComponent)
    childCmp: ChildComponent;

    ngAfterContentInit() {
        console.dir(this.childCmp);
    }
}
```


app.component.ts
```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <h4>Welcome to Angular World</h4>
    <exe-parent>
      <exe-child></exe-child>
    </exe-parent>
  `,
})
export class AppComponent { }
```

以上代码运行后，控制台的输出结果：

![ContentChild的例子](https://sfault-image.b0.upaiyun.com/120/198/1201986887-58c91aa3db0f3_articlex)


parent.component.ts
```typescript
import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { ChildComponent } from './child.component';

@Component({
    selector: 'exe-parent',
    template: `
      <p>Parent Component</p>  
      <ng-content></ng-content>
    `
})
export class ParentComponent implements AfterContentInit {
    
    @ContentChildren(ChildComponent)
    childCmps: QueryList<ChildComponent>;

    ngAfterContentInit() {
        console.dir(this.childCmps);
    }
}
```

app.component.ts
```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <h4>Welcome to Angular World</h4>
    <exe-parent>
      <exe-child></exe-child>
      <exe-child></exe-child>
    </exe-parent>
  `,
})
export class AppComponent { }
```

以上代码运行后，控制台的输出结果：

![ContentChildren的例子](https://sfault-image.b0.upaiyun.com/181/988/1819881793-58c91ab30b9fb_articlex)


#### @ViewChild & @ViewChildren

这一小节[参考文章](https://segmentfault.com/a/1190000008695459)《Angular 2 ViewChild & ViewChildren》

1. ViewChild 是属性装饰器，用来从模板视图中获取匹配的元素。视图查询在 ngAfterViewInit 钩子函数调用前完成，因此在 ngAfterViewInit 钩子函数中，才能正确获取查询的元素。
2. ViewChildren 用来从模板视图中获取匹配的多个元素，返回的结果是一个 QueryList 集合。


#### @HostListener & @HostBinding

这一小节[参考文章](https://segmentfault.com/a/1190000008878888)《Angular 2 HostListener & HostBinding》

1. @HostListener 是属性装饰器，用来为宿主元素添加事件监听
2. @HostBinding 也是属性装饰器，用来动态设置宿主元素的属性值。








